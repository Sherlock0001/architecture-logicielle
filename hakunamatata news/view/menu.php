<aside class="col-xs-12 col-sm-12 col-md-4 col-lg-3 col-xl-3 tm-aside-r">
    <div class="tm-aside-container">
        <h3 class="tm-gold-text tm-title">Catégories</h3>
        <nav>
            <ul class="nav">
                <li><a href="index_.php" class="tm-text-link">Accueil</a></li>
                <?php foreach ($categories as $categorie): ?>
                    <li><a href="index_.php?action=categorie&id=<?= $categorie->idCategorie ?>" class="tm-text-link"><?= $categorie->nomCategorie ?></a></li>
                <?php endforeach ?>
            </ul>
        </nav>                     
    </div> 
</aside>
