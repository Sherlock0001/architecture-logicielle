<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- load stylesheets -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400">  <!-- Google web font "Open Sans" -->
    <link rel="stylesheet" href="css/bootstrap.min.css">                                      <!-- Bootstrap style -->
    <link rel="stylesheet" href="css/templatemo-style.css">                                   <!-- Templatemo style -->
</head>

 <body>
        
        <?php require_once 'header.php'; ?>

        <section class="tm-section">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-9 col-xl-9">
                        <div class="row tm-margin-t-big">

                            <?php if (!empty($article)): ?>
                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4" style="width: 50%">
                                        <div class="tm-content-box">
                                            <?php echo '<img src="' .$article->image. '" alt="Image" class="tm-margin-b-30 img-fluid"/>'; ?>
                                            <h4 class="tm-margin-b-20 tm-gold-text">
                                                <?php echo $article->titre; ?>
                                            </h4>
                                            <h4 class="tm-margin-b-20 tm-gold-text">
                                                <?php echo $article->auteur; ?>
                                            </h4>
                                            <p class="tm-margin-b-20">
                                                <?php echo $article->description; ?>
                                            </p>
                                        </div>  
                                </div>
                            <?php else: ?>
                                <h2>Article non trouvé</h2>
                            <?php endif ?>
                            
                        </div>
                    </div>

                    <?php require_once 'menu.php'; ?>

                </div>
            </div>
        </section>

        <!-- load JS files -->
        <script src="js/jquery-1.11.3.min.js"></script>             <!-- jQuery (https://jquery.com/download/) -->
        <script src="https://www.atlasestateagents.co.uk/javascript/tether.min.js"></script> <!-- Tether for Bootstrap, http://stackoverflow.com/questions/34567939/how-to-fix-the-error-error-bootstrap-tooltips-require-tether-http-github-h --> 
        <script src="js/bootstrap.min.js"></script>                 <!-- Bootstrap (http://v4-alpha.getbootstrap.com/) -->
       
</body>
</html>