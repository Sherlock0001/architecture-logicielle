create database News;

use News;

create table Categorie(
     idCategorie int AUTO_INCREMENT PRIMARY KEY,
     NomCategorie varchar(100)
)ENGINE=InnoDB;

create table Administrateur(
     identity int AUTO_INCREMENT PRIMARY KEY,
     id varchar(32),
     password varchar(32)
)ENGINE=InnoDB;

create table Editeur(
     id int AUTO_INCREMENT PRIMARY KEY,
     login varchar(32),
     password varchar(32)
)ENGINE=InnoDB;

create table Article (
     idArticle int AUTO_INCREMENT PRIMARY KEY,
     Categorie int,
     Titre varchar(255),
     Auteur varchar(255),
     Description varchar(255),
     Image varchar(255),
     CONSTRAINT fk_Article_Ctgrie       
     FOREIGN KEY (Categorie)             
     REFERENCES Categorie(idCategorie)
)ENGINE=InnoDB;

insert into Categorie values("","cuisine");
insert into Categorie values("","mode");
insert into Categorie values("","sport");

insert into article values ("","3","Neymar au Bar�a","Etienne Moatti",
	"Financi�rement, le retour de la star br�silienne en Catalogne est une op�ration compliqu�e, mais pas impossible � r�aliser.",
	"C:\wamp\www\dic\hakunamatata news\img\barca.jpg");

insert into article values ("","3","Wimbledon: Federer t�te de s�rie no2 devant Nadal","Magnus Norman",
	"Comme pr�vu, Roger Federer (ATP 3) a �t� d�sign� t�te de s�rie no2 du tournoi de Wimbledon (1-14 juillet). Le Grand Chelem londonien, qui �tablit son propre bar�me, a plac� le B�lois devant Rafael Nadal, bien que l'Espagnol soit 2e au classement mondial.",
	"C:\wamp\www\dic\hakunamatata news\img\federer.png");

insert into article values ("","3","Victoire des Raptors: Drake euphorique","WENN",
	"Drake est plus qu�heureux d�avoir vu les Raptors de Toronto, l��quipe de basket qu�il supporte, remporter la finale de la NBA en prenant le dessus sur les Golden State Warriors.",
	"C:\wamp\www\dic\hakunamatata news\img\drake.jpg");

insert into article values ("","3","Can 2019 : S�n�gal-Alg�rie, l�historique des confrontations entre Fennecs et Lions","wiwsport",
	"A deux jours, du duel tant attendu par les amoureux du ballon rond opposant les fennecs d�Alg�rie aux lions de la Teranga , comptant pour la deuxi�me journ�e de la poule C, wiwsport.com vous replonge sur l�historique des confrontations du S�n�gal avec l�Alg�rie.",
	"C:\wamp\www\dic\hakunamatata news\img\sen-alg.jpg");

insert into article values ("","2","Robe en wax pour l'�t�","Aicha WAR",
     "Le wax est une mati�re faite en coton alors ne vous faites pas prier et cousez vos mod�les amples de robes Les couleurs sont l� et les id�es ne manquent pas. Sus aux jeans et vive le confort!",
     "./img/robe1.jpg");

insert into article values ("","2","Pantalon en wax","Julia DIAGNE",
     "Le pantalon est l'habit le plus classe en entreprise et le plus relax � la maison. Il y'en a pour tous les styles et la limite c'est votre imagination.",
     "./img/pantalon.jpg");

insert into article values ("","2","Jupes en wax","Jedda BEYATT",
     "Le wax, mondialement reconnu, est le tissu-tendance qui fait des ravages dans la mode s�n�galaise actuelle. Tout ce qui vous reste � faire c'est de choisir votre mod�le: jupe 3/4, jupe longue, jupes tailleurs, jupes crayons, etc...",
     "./img/jupe.jpg");

insert into article values ("","2","Hauts en wax","Sophie MENDY",
     "Pour un style vestimentaire original et charmant, optez pour des hauts en wax toujours dans l'optique de vous soulager de la chaleur actuelle. Le catalogue est vaste, par exemple vous pouvez consulter pinterest pour plus d'inspiration.",
     "./img/haut.jpg");

insert into article values ("","1","Ingr�dients du ceebu dieune","Jedda BEYATT",
     "Pour faire du bon ceebu dieune il vous faut les ingr�dients suivants: du poisson (m�rou ou thioff de pr�f�rence), l�gumes(carrote, choux, navet, aubergine, tamarin, piment, poivron, oignons, oignons verts, ail), riz, huile, sel, persil, pur�e de tomate.",
     "./img/ceeb.jpg");

insert into article values ("","1","Le yassa guinar: un plat sain?","Sophie MENDY",
     "Le yassa est souvent d�valu� alors qu'il a beaucoup de potentiel et peut m�me �re le plat s�n�galais le plus sain. Comment donc? D'abord il faut privil�gier la barbercue au lieu de la friture, ensuite diminuer la quantit� d'huile lors de la cuisson. Enfin, colorez votre plat avec des l�gumes! Oui des l�gumes!! Ce sont de bons anti-oxydants mais � condition de les cuire � la vapeur. ",
     "./img/yassa.jpg");

insert into article values ("","1","Ceebu yapp: plat des grandes c�r�monies","Aicha WAR",
     "La r�alit� de notre pays est telle que nous pour chaque contexte de c�r�monie, la viande ou le poulet est le meilleur condiment des plats. Et donc le riz � la viande est le plat royal des c�r�monies. Il y'a plusieurs variantes, avec sauce ou sans sauce, avec mac�doine de l�gumes ou non. Et il fait le r�gal des s�n�galais.",
     "./img/ceeb_yapp.jpg");

insert into article values ("","1","Le soupou kandia un vrai d�lice","Julia DIAGNE",
     "Le soupou kandia est un plat d�licieux � base de gombo et d'huile de palme. Il fait le bonheur des diolas amateurs de niankatang, des s�r�res amoureux du ceeb et des peulhs amoureux de sauce.",
     "./img/soupou-kandia.jpg");

insert into Administrateur values ("", "waraicha34@gmail.com", "soap_project");