<?php
	
	require_once 'service.php';

	class Administrateur
	{

		public $id;
		public $identity;
		public $password;
		
		function __construct(){

		}

		public function getAdmins(){
			$service = new Service();
			return $service->getAdmins();
		}

		public function getAdmin($id){
			$service = new Service();
			$tab = array('identity'=>$id);
			return $service->getAdmin($tab);
		}

		public function delete($id){
			$service = new Service();
			$tab = array('identity'=>$id);
			return $service->getAdmin($tab);
		}

		public function update($a){
			$service = new Service();
			$admin = array('admin'=>$a);
			return $service->updateAdmin($admin);
		}

		public function addAdmin($adm){
			$service = new Service();
			$admin = array('admin'=>$adm);
			return $service->addAdmin($admin);
		}
	}
?>