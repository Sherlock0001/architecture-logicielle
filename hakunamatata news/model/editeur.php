<?php
	
	require_once 'service.php';

	class Editeur
	{

		public $id;
		public $login;
		public $password;
		

		function __construct(){
			
		}

		public function getEditeurs(){ 
			$service = new Service();
			return $service->getEditeurs();
		}

		public function getEditeur($id){
			$service = new Service();
			$tab = array('id'=>$id); 
			return $service->getEditeur($tab);
		}

		public function delete($id){
			$service = new Service();
			$tab = array('id'=>$id); 
			return $service->deleteEditeur($tab);
		}

		public function update($ed){
			$service = new Service();
			$editeur = array('editeur'=>$ed);
			return $service->updateEditeur($editeur);
		}

		public function addEditeur($ed){
			$service = new Service();
			$editeur = array('editeur'=>$ed);
			return $service->addEditeur($editeur);
		}
	}
?>