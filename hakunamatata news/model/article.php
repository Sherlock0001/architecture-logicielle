<?php
	require_once 'service.php';

	class Article
	{

		public $image;
		public $titre;
		public $auteur;
		public $categorie;
		public $idArticle;
		public $description;


		function __construct()
		{
			
		}


		public function getArticles(){
			$service = new Service();
			return $service->getArticles();
		}


		public function getArticle($id){
			$service = new Service();
			$tab = array('idArticle'=>$id); 
			return $service->getArticle($tab);
		}


		public function getArticleCategory($id){
			$service = new Service();
			$tab = array('categorie'=>$id); 
			return $service->getArticleCategory($tab);
		}


		public function delete($id){
			$service = new Service();
			$tab = array('idArticle'=>$id); 
			return $service->deleteArticle($tab);
		}


		public function update($art){
			$service = new Service();
			$article = array('article'=>$art);
			return $service->deleteArticle($article);
		}


		public function addArticle($art){
			$service = new Service();
			$article = array('article'=>$art);
			return $service->addArticle($article);
		}

	}
?>