<?php

	class Service
	{

		public function __construct()
		{
			$this->instance = new SoapClient("http://localhost:2017/GestionWS?WSDL", array('wsdl_cache' => 0, 'trace' => 1));
		}


		public function getAdmins(){
			try {
			    return $this->instance->listAdmin()->return;
			} catch (SoapFault $e) {
			    print($this->instance->__getLastResponse());
			}
		}

		public function getAdmin($tab){
			try {
			    return $this->instance->findAdmin($tab)->return;
			} catch (SoapFault $e) {
			    print($this->instance->__getLastResponse());
			}
		}

		public function deleteAdmin($tab){
			try {
			    $this->instance->deleteAdmin($tab);
			} catch (SoapFault $e) {
			    print($this->instance->__getLastResponse());
			}
		}

		public function updateAdmin($admin){
			try {
			    $this->instance->updateAdmin($admin);
			} catch (SoapFault $e) {
			    print($this->instance->__getLastResponse());
			}
		}

		public function addAdmin($admin){
			try {
			    $this->instance->addAdmin($admin);
			} catch (SoapFault $e) {
			    print($this->instance->__getLastResponse());
			}
		}

		public function getArticles(){
			try {
			    return $this->instance->listArticle()->return;
			} catch (SoapFault $e) {
			    print($this->instance->__getLastResponse());
			}		
		}

		public function getArticle($tab){
			try {
			    return $this->instance->findArticle($tab)->return;
			} catch (SoapFault $e) {
			    print($this->instance->__getLastResponse());
			}
		}

		public function getArticleCategory($tab){
			try {
			    return $this->instance->findArticleCategory($tab)->return;
			} catch (SoapFault $e) {
			    print($this->instance->__getLastResponse());
			}
		}

		public function deleteArticle($tab){
			try {
			    $this->instance->deleteArticle($tab);
			} catch (SoapFault $e) {
			    print($this->instance->__getLastResponse());
			}
		}

		public function updateArticle($article){
			try {
			    $this->instance->updateArticle($article);
			} catch (SoapFault $e) {
			    print($this->instance->__getLastResponse());
			}
		}

		public function addArticle($tab){
			try {
			    $this->instance->addArticle($tab);
			} catch (SoapFault $e) {
			    print($this->instance->__getLastResponse());
			}
		}

		public function getCategories(){
			try {
			    return $this->instance->listCategorie()->return;
			} catch (SoapFault $e) {
			    print($this->instance->__getLastResponse());
			}
		}

		public function getCategorie($tab){
			try {
			    return $this->instance->findCategorie($tab)->return;
			} catch (SoapFault $e) {
			    print($this->instance->__getLastResponse());
			}
		}

		public function deleteCategorie($tab){
			try {
			    $this->instance->deleteCategorie($tab);
			} catch (SoapFault $e) {
			    print($this->instance->__getLastResponse());
			}
		}

		public function updateCategorie($categorie){
			try {
			    $this->instance->updateCategorie($categorie);
			} catch (SoapFault $e) {
			    print($this->instance->__getLastResponse());
			}
		}

		public function addCategorie($categorie){
			try {
			    $this->instance->addCategorie($categorie);
			} catch (SoapFault $e) {
			    print($this->instance->__getLastResponse());
			}
		}

		public function getEditeurs($tab){
			try {
			    return $this->instance->listEditeur()->return;
			} catch (SoapFault $e) {
			    print($this->instance->__getLastResponse());
			}
		}

		public function getEditeur($tab){
			try {
			    return $this->instance->findEditeur($tab)->return;
			} catch (SoapFault $e) {
			    print($this->instance->__getLastResponse());
			}
		}

		public function deleteEditeur($tab){
			try {
			    $this->instance->deleteEditeur($tab);
			} catch (SoapFault $e) {
			    print($this->instance->__getLastResponse());
			}
		}

		public function updateEditeur($editeur){
			try {
			    $this->instance->updateEditeur($editeur);
			} catch (SoapFault $e) {
			    print($this->instance->__getLastResponse());
			}
		}

		public function addEditeur($editeur){
			try {
			    $this->instance->addEditeur($editeur);
			} catch (SoapFault $e) {
			    print($this->instance->__getLastResponse());
			}
		}
		
	}

?>