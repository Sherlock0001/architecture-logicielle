<?php
	
	require_once 'service.php';

	class Categorie
	{
		public $idCategorie;
		public $nomCategorie;


		function __construct()
		{
			
		}


		public function getCategories(){
			$service = new Service();
			return $service->getCategories();
		}

		public function getCategory($id){
			$service = new Service();
			$tab = array('idCategorie'=>$id); 
			return $service->getCategorie($tab);
		}

		public function delete($id){
			$service = new Service();
			$tab = array('idCategorie'=>$id); 
			return $service->deleteCategorie($tab);
		}

		public function update($cat){
			$service = new Service();
			$categorie = array('categorie'=>$cat);
			return $service->updateCategorie($categorie);

		}

		public function addCategory($cat){
			$service = new Service();
			$categorie = array('categorie'=>$cat);
			return $service->addCategorie($categorie);
		}
	}
?>