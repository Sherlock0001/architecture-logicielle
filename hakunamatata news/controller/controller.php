<?php
	require_once 'model/editeur.php';
	require_once 'model/article.php';
	require_once 'model/categorie.php';
	require_once 'model/administrateur.php';

	class Controller
	{
		
		function __construct()
		{
			
		}

		public function showAccueil()
		{
			$article = new Article();
			$categorie = new Categorie();
			$articles = $article->getArticles();
			$categories = $categorie->getCategories();
			require_once 'view/accueil.php';
		}

		public function showArticle($id)
		{
			$article = new Article();
			$categorie = new Categorie();
			$article = $article->getArticle($id);
			$categories = $categorie->getCategories();
			require_once 'view/article.php';
		}

		public function showCategorie($id)
		{
			$article = new Article();
			$categorie = new Categorie();
			$articles = $article->getArticleCategory($id);
			$categories = $categorie->getCategories();
			require_once 'view/articlesCategorie.php';
		}

		public function addArticle($id, $titre, $auteur, $description, $categorie, $image){
			$article = new Article();
			$article->titre = $titre;
			$article->auteur = $auteur;
			$article->categorie = $categorie;
			$article->image = $image;
			$article->description = $description;
			$article->addArticle($article);
			require_once '#';
		}

		public function updateArticle($id, $titre, $auteur, $description, $categorie, $image){
			$article = new Article();
			$article->idArticle = $id;
			$article->titre = $titre;
			$article->auteur = $auteur;
			$article->categorie = $categorie;
			$article->image = $image;
			$article->description = $description;
			$article->updateArticle($article);
			require_once '#';
		}

		public function deleteArticle($id){
			$article = new Article();
			$article->deleteArticle($id);
			require_once '#';
		}

		public function addAdmin($login, $pwd){
			$admin = new Administrateur();
			$admin->id = $login;
			$admin->password = $pwd;
			$admin->addAdmin($admin);
			require_once '#';
		}

		public function updateAdmin($id, $login, $pwd){
			$admin = new Administrateur();
			$admin->id = $login;
			$admin->identity = $id;
			$admin->password = $pwd;
			$admin->updateAdmin($admin);
			require_once '#';
		}

		public function deleteAdmin($id){
			$admin = new Administrateur();
			$admin->deleteAdmin($id);
			require_once '#';
		}

	}
?>