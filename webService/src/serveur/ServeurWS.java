package serveur;

import javax.xml.ws.Endpoint;
import service.GestionWS;

public class ServeurWS{

	public static void main(String[] args) {
		Endpoint.publish("http://localhost:2019/", new GestionWS());
	}

}
