package service;

import java.sql.DriverManager;
import domaine.Administrateur;
import java.util.ArrayList;
import domaine.Categorie;
import domaine.Editeur;
import domaine.Article;
import javax.jws.*;

@WebService
public class GestionWS {

	private java.sql.ResultSet rs=null; 
	private java.sql.Connection con=null;
	private java.sql.PreparedStatement st=null;
	
	public GestionWS() {
		try
	    {
	      Class.forName("com.mysql.jdbc.Driver");
	 	  con=DriverManager.getConnection("jdbc:mysql://localhost:3306/news","root","");
	    }
	    catch(Exception e)
	    { System.out.println(e.getMessage()); }
	}
	
	@WebMethod
	public void addAdmin(@WebParam(name="admin") Administrateur a) {
		try
		 {
			st = con.prepareStatement("insert into administrateur(id, password)  values(?,?)");
			st.setString(1,a.getId());
			st.setString(2,a.getPassword());
			st.executeUpdate();
		  }catch(Exception e){
			  	System.out.println(e.getMessage());
		  }
	}

	@WebMethod
	public void updateAdmin(@WebParam(name="admin") Administrateur a) {
		try
		 {
			st = con.prepareStatement("update administrateur set id = ?, password= ? where identity = ?");
			st.setString(1,a.getId());
			st.setString(2,a.getPassword());
			st.setInt(3,a.getIdentity());
			st.executeUpdate();
		  }catch(Exception e){
			  	System.out.println(e.getMessage());
		  }
	}

	@WebMethod
	public void deleteAdmin(@WebParam(name="identity") int identity) {
		try
		 {
			st = con.prepareStatement("delete from administrateur where identity = ?");
			st.setInt(1,identity);
			st.executeUpdate();
		  }catch(Exception e){
			  	System.out.println(e.getMessage());
		  }
	}

	@WebMethod
	public ArrayList<Administrateur> listAdmin() {
		ArrayList<Administrateur> admins = new ArrayList<Administrateur>();
		try
	     {
	       st=con.prepareStatement("select * from administrateur");
	       rs=st.executeQuery();
	       while(rs.next())
	         {
	           Administrateur a = new Administrateur();
	           a.setId(rs.getString("id"));
	           a.setPassword(rs.getString("password"));
	           a.setIdentity(rs.getInt("identity"));
	           admins.add(a);
	         }
	      }catch(Exception e)
		  { System.out.println(e.getMessage()); }
		return admins;
	}

	@WebMethod
	public Administrateur findAdmin(@WebParam(name="identity") int identity) {
		Administrateur a=null ;
	     try
	     {
	       st=con.prepareStatement("select * from administrateur where identity = ?");
	       st.setInt(1,identity);
	       rs=st.executeQuery();
	       if(rs.next())
	         {
	           a = new Administrateur();
	           a.setIdentity(rs.getInt("identity"));
	           a.setId(rs.getString("id"));
	           a.setPassword(rs.getString("password"));
	         }
	       }catch(Exception e)
	       { System.out.println(e.getMessage()); }
	    return a;
	}

	@WebMethod
	public void addArticle(@WebParam(name="article") Article a) {
		try
		 {
			st = con.prepareStatement("insert into article(Categorie, Titre, Auteur, Description, Image) values(?,?,?,?,?)");
			st.setInt(1,a.getCategorie());
			st.setString(2,a.getTitre());
			st.setString(3,a.getAuteur());
			st.setString(4,a.getDescription());
			st.setString(5,a.getImage());
			st.executeUpdate();
		  }catch(Exception e){
			  	System.out.println(e.getMessage());
		  }
	}

	@WebMethod
	public void updateArticle(@WebParam(name="article") Article a) {
		try
		 {
			st = con.prepareStatement("update article set categorie=?, titre=?, auteur=?, description=?, image=? where idArticle = ?");
			st.setInt(1,a.getCategorie());
			st.setString(2,a.getTitre());
			st.setString(3,a.getAuteur());
			st.setString(4,a.getDescription());
			st.setString(5,a.getImage());
			st.setInt(6,a.getIdArticle());
			st.executeUpdate();
		  }catch(Exception e){
			  	System.out.println(e.getMessage());
		  }
	}

	@WebMethod
	public void deleteArticle(@WebParam(name="idArticle") int idArticle) {
		try
		 {
			st = con.prepareStatement("delete from article where idArticle = ?");
			st.setInt(1,idArticle);
			st.executeUpdate();
		  }catch(Exception e){
			  	System.out.println(e.getMessage());
		  }
	}

	@WebMethod
	public ArrayList<Article> listArticle() {
		ArrayList<Article> articles = new ArrayList<Article>();
		try
	     {
	       st=con.prepareStatement("select * from article");
	       rs=st.executeQuery();
	       while(rs.next())
	         {
	           Article a = new Article();
	           a.setIdArticle(rs.getInt("idArticle"));
	           a.setCategorie(rs.getInt("Categorie"));
	           a.setTitre(rs.getString("Titre"));
	           a.setAuteur(rs.getString("Auteur"));
	           a.setDescription(rs.getString("Description"));
	           a.setImage(rs.getString("Image"));
	           articles.add(a);
	         }
	      }catch(Exception e)
		  { System.out.println(e.getMessage()); }
		return articles;
	}

	@WebMethod
	public Article findArticle(@WebParam(name="idArticle") int idArticle) {
		Article a=null ;
	     try
	     {
	       st=con.prepareStatement("select * from article where idArticle = ?");
	       st.setInt(1,idArticle);
	       rs=st.executeQuery();
	       if(rs.next())
	         {
	           a = new Article();
	           a.setIdArticle(rs.getInt("idArticle"));
	           a.setCategorie(rs.getInt("Categorie"));;
	           a.setTitre(rs.getString("Titre"));
	           a.setAuteur(rs.getString("Auteur"));
	           a.setDescription(rs.getString("Description"));
	           a.setImage(rs.getString("Image"));
	         }
	       }catch(Exception e)
	       { System.out.println(e.getMessage()); }
	    return a;
	}

	@WebMethod
	public ArrayList<Article> findArticleCategory(int categorie) {
		ArrayList<Article> articlesCategorie = new ArrayList<Article>();
		try
	     {
	       st=con.prepareStatement("select * from article where categorie = ?");
	       st.setInt(1,categorie);
	       rs=st.executeQuery();
	       while(rs.next())
	         {
	           Article a = new Article();
	           a.setIdArticle(rs.getInt("idArticle"));
	           a.setCategorie(rs.getInt("Categorie"));
	           a.setTitre(rs.getString("Titre"));
	           a.setAuteur(rs.getString("Auteur"));
	           a.setDescription(rs.getString("Description"));
	           a.setImage(rs.getString("Image"));
	           articlesCategorie.add(a);
	         }
	      }catch(Exception e)
		  { System.out.println(e.getMessage()); }
		return articlesCategorie;
	}

	@WebMethod
	public void addCategorie(@WebParam(name="categorie") Categorie c) {
		try
		 {
			st = con.prepareStatement("insert into categorie(nomCategorie)  values(?)");
			st.setString(1,c.getNomCategorie());
			st.executeUpdate();
		  }catch(Exception e){
			  	System.out.println(e.getMessage());
		  }
	}

	@WebMethod
	public void updateCategorie(@WebParam(name="categorie") Categorie c) {
		try
		 {
			st = con.prepareStatement("update categorie set nomcategorie=? where idCategorie = ?");
			st.setString(1,c.getNomCategorie());
			st.setInt(2,c.getIdCategorie());
			st.executeUpdate();
		  }catch(Exception e){
			  	System.out.println(e.getMessage());
		  }
	}

	@WebMethod
	public void deleteCategorie(@WebParam(name="idCategorie") int idCategorie) {
		try
		 {
			st = con.prepareStatement("delete from categorie where idCategorie = ?");
			st.setInt(1,idCategorie);
			st.executeUpdate();
		  }catch(Exception e){
			  	System.out.println(e.getMessage());
		  }
	}

	@WebMethod
	public ArrayList<Categorie> listCategorie() {
		ArrayList<Categorie> categories = new ArrayList<Categorie>();
		try
	     {
	       st=con.prepareStatement("select * from categorie");
	       rs=st.executeQuery();
	       while(rs.next())
	         {
	    	   Categorie c = new Categorie();
	           c.setIdCategorie(rs.getInt("idCategorie"));
	           c.setNomCategorie(rs.getString("nomcategorie"));
	           categories.add(c);
	         }
	      }catch(Exception e)
		  { System.out.println(e.getMessage()); }
		return categories;
	}

	@WebMethod
	public Categorie findCategorie(@WebParam(name="idCategorie") int idCategorie) {
		Categorie c=null ;
	     try
	     {
	       st=con.prepareStatement("select * from categorie where idCategorie = ?");
	       st.setInt(1,idCategorie);
	       rs=st.executeQuery();
	       if(rs.next())
	         {
	           c = new Categorie();
	           c.setIdCategorie(rs.getInt("idCategorie"));
	           c.setNomCategorie((rs.getString("nomcategorie")));
	         }
	       }catch(Exception e)
	       { System.out.println(e.getMessage()); }
	    return c;
	}

	@WebMethod
	public void addEditeur(@WebParam(name="editeur") Editeur e) {
		try
		 {
			st = con.prepareStatement("insert into editeur(login, password)  values(?,?)");
			st.setString(1,e.getLogin());
			st.setString(2,e.getPassword());
			st.executeUpdate();
		  }catch(Exception k){
			  	System.out.println(k.getMessage());
		  }
	}

	@WebMethod
	public void updateEditeur(@WebParam(name="editeur") Editeur e) {
		try
		 {
			st = con.prepareStatement("update editeur set login=?, password=? where id = ?");
			st.setString(1,e.getLogin());
			st.setString(2,e.getPassword());
			st.setInt(3,e.getId());
			st.executeUpdate();
		  }catch(Exception k){
			  	System.out.println(k.getMessage());
		  }
	}

	@WebMethod
	public void deleteEditeur(@WebParam(name="id") int id) {
		try
		 {
			st = con.prepareStatement("delete from editeur where id = ?");
			st.setInt(1,id);
			st.executeUpdate();
		  }catch(Exception k){
			  	System.out.println(k.getMessage());
		  }
	}

	@WebMethod
	public ArrayList<Editeur> listEditeur() {
		ArrayList<Editeur> editeurs = new ArrayList<Editeur>();
		try
	     {
	       st=con.prepareStatement("select * from editeur");
	       rs=st.executeQuery();
	       while(rs.next())
	         {
	    	   Editeur e = new Editeur();
	           e.setId(rs.getInt("id"));
	           e.setLogin(rs.getString("login"));
	           e.setPassword(rs.getString("password"));
	           editeurs.add(e);
	         }
	      }catch(Exception k)
		  { System.out.println(k.getMessage()); }
		return editeurs;
	}

	@WebMethod
	public Editeur findEditeur(@WebParam(name="id") int id) {
		Editeur e=null ;
	     try
	     {
	       st=con.prepareStatement("select * from editeur where id = ?");
	       st.setInt(1,id);
	       rs=st.executeQuery();
	       if(rs.next())
	         {
	           e = new Editeur();
	           e.setId(rs.getInt("id"));
	           e.setLogin((rs.getString("login")));
	           e.setPassword((rs.getString("password")));
	         }
	       }catch(Exception k)
	       { System.out.println(k.getMessage()); }
	    return e;
	}
	
}
