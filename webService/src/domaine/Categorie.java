package domaine;

public class Categorie {

	private int idCategorie;
	private String nomCategorie;
	
	public int getIdCategorie() {
		return idCategorie;
	}

	public void setIdCategorie(int id) {
		this.idCategorie = id;
	}

	public String getNomCategorie() {
		return nomCategorie;
	}

	public void setNomCategorie(String nomCategorie) {
		this.nomCategorie = nomCategorie;
	}

	public Categorie() {
		
	}
	
}
