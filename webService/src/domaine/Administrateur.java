package domaine;

public class Administrateur {

	private String id;
	private int identity;
	private String password;
	

	public int getIdentity() {
		return identity;
	}

	public void setIdentity(int id) {
		this.identity = id;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Administrateur(){
	}
}
