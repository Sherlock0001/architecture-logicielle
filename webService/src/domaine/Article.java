package domaine;

public class Article {

	private int idArticle;
	private String image;
	private String titre;
	private String auteur;
	private String description;
	private int categorie;
	
	public Article() {
		
	}

	public int getIdArticle() {
		return idArticle;
	}

	public void setIdArticle(int id) {
		this.idArticle = id;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public String getAuteur() {
		return auteur;
	}

	public void setAuteur(String auteur) {
		this.auteur = auteur;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getCategorie() {
		return this.categorie;
	}

	public void setCategorie(int idcategorie) {
		this.categorie = idcategorie;
	}
}
