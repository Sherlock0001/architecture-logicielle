package service;

import java.util.ArrayList;
import java.util.Scanner;

public class Administration {

	public Administration() {
		
	}
	
	//FONCTION DE RECUPERATION DES PARAMETRES DE CONNEXION ET DE COORDINATION
	public void menu(GestionWS s) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Veuillez entrer votre login:");
		String id = sc.next();
		System.out.println("Maintenant donnez votre mot de passe:");
		String pwd = sc.next();
		System.out.println("\nPatientez...\n");
		
		if(verifStatut(s,id,pwd)) 
			  boucle(s);
		else 
			System.out.println("Vous ne figurez pas dans notre listre d administration!");
	}
	
	//BOUCLE SUR LE MENU DE GESTION DES UTILISATEURS
	public void boucle(GestionWS s) {
		do {
			action(choix(), s);
		}while(true);
	}
	
	//RETOURNE LE CHOIX DU MENU
	public int choix() {
		Scanner sc = new Scanner(System.in);
		int choix=0;
		do {
			System.out.println("Choisissez une action:");
			System.out.println("1 -> Lister les editeurs:");
			System.out.println("2 -> Ajouter un editeur:");
			System.out.println("3 -> Supprimer un editeur:");
			System.out.println("4 -> Modifier un editeur:");
			System.out.println("5 -> Rechercher un editeur:\n");
			choix = sc.nextInt();
		}while(choix<1 || choix>5);
		return choix;
	}
	
	//APPELLE LES FONCTIONS LIEES AUX CHOIX DE L'ADMINISTRATEUR
	public void action(int choix, GestionWS s) {
		switch(choix) {
			case 1 : listEditors(s);
			 		 break;
			case 2 : addEditor(s);
					 break;
			case 3 : deleteEditor(s);
					 break;
			case 4 : updateEditor(s);
					 break;
			default: recherche(s);
					 break;
		}
	}
	
	//AUTHENTIFICATION DE L'ADMINISTRATEUR
	public boolean verifStatut(GestionWS s, String id, String pwd) {
		ArrayList<Administrateur> a = (ArrayList<Administrateur>) s.listAdmin();
		for(Administrateur admin : a) {
			if(admin.getId().equals(id) && admin.getPassword().equals(pwd)) {
				return true;
			}
		}
		return false;
	}
	
	//RECHERCHE UN EDITEUR
	public void recherche(GestionWS s) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Donnez la clef du sujet que vous cherchez:");
		int id = sc.nextInt();
		try {
			Editeur e = s.findEditeur(id);
			System.out.println("\n=========================");
			System.out.println("Login : "+ e.getLogin());
			System.out.println("Password : "+ e.getLogin()); 
			System.out.println("=========================\n");
		}catch(Exception e) {
			System.out.println("Editeur inexistant!");
		}
	}
	
	//AFFICHE TOUS LES EDITEURS
	
	public void listEditors(GestionWS s) {
		ArrayList<Editeur> e = (ArrayList<Editeur>) s.listEditeur();
		for(Editeur ed : e) {
			System.out.println("==============\nEditeur "+ed.getId()+"\n==============");
			System.out.println(ed.getLogin()+"\n"+ed.getPassword());
		}
	}

	
	//AJOUTE UN EDITEUR
	public void addEditor(GestionWS s) {
		Scanner sc = new Scanner(System.in);
		String login, pwd;
		do {
			System.out.println("Donnez le login du sujet a ajouter:");
			login = sc.next();
		}while(login.length()<3);
		do {
			System.out.println("Donnez le mot de passe du sujet a ajouter:");
			pwd = sc.next();
		}while(pwd.length()<3
				);
		Editeur e = new Editeur();
		e.setPassword(pwd);
		e.setLogin(login);
		try {
			s.addEditeur(e);
			System.out.println("Ajout effectue!");
		}catch(Exception exc) {
			System.out.println("Ajout non effectue!");
		}
	}
	
	
	//SUPPRIME UN EDITEUR
	public void deleteEditor(GestionWS s) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Donnez la clef du sujet a supprimer:");
		int id = sc.nextInt();
		try {
			s.deleteEditeur(id);
			System.out.println("Suppression effectuee!");
		}catch(Exception e) {
			System.out.println("Editeur inexistant!");
		}
	}
	
	//MET A JOUR LES INFOS D'UN EDITEUR

	public void updateEditor(GestionWS s) {
		Scanner sc = new Scanner(System.in);
		String login, pwd;
		int id;
		do {
			System.out.println("Donnez la clef du sujet a modifier:");
			id = sc.nextInt();
		}while(id<0);
		do {
			System.out.println("Donnez le login du sujet a modifier:");
			login = sc.next();
		}while(login.length()<3);
		do {
			System.out.println("Donnez le mot de passe du sujet a modifier:");
			pwd = sc.next();
		}while(pwd.length()<3);
		Editeur e = new Editeur();
		e.setPassword(pwd);
		e.setLogin(login);
		e.setId(id);
		try {
			s.updateEditeur(e);
			System.out.println("Mise a jour effectuee!");
		}catch(Exception exc) {
			System.out.println("Editeur inexistant!");
		}
	}
}
