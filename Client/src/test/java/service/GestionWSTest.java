package service;

import java.util.ArrayList;

import org.junit.Test;

public class GestionWSTest {
	
	public void findArticle(GestionWS s, int id) {
		Article article = s.findArticle(2);
		System.out.println("==============\nArticle ID = "+id+"\n==============\n"+article.titre+"\n"+article.auteur+"\n"+article.description+"\n");
		System.out.println();
	}
	
	public void addAdmin(GestionWS s, String login, String pwd) {
		Administrateur admin = new Administrateur();
		admin.setId(login);
		admin.setPassword(pwd);
		s.addAdmin(admin);
	}
	
	public void listAdmin(GestionWS s) {
		ArrayList<Administrateur> a = (ArrayList<Administrateur>) s.listAdmin();
		for(Administrateur admin : a) {
			System.out.println("==============\nAdministrateur "+admin.getIdentity()+"\n==============");
			System.out.println(admin.getId()+"\n"+admin.getPassword());
			System.out.println();
		}
	}
	
	public void updateCategorie(GestionWS s, String nom, int id) {
		Categorie c = new Categorie();
		c.setIdCategorie(id);
		c.setNomCategorie(nom);
		s.updateCategorie(c);
	}
	
	public void ArticleCategorie(GestionWS s, int idCat) {
		ArrayList<Article> a = (ArrayList<Article>) s.findArticleCategory(idCat);
		for(Article art : a) {
			System.out.println("==============\nArticle "+art.getIdArticle()+"\n==============");
			System.out.println(art.getTitre()+"\n"+art.getAuteur()+"\n"+art.getDescription());
			System.out.println();
		}
	}

	@Test
	public void test() {
		
		GestionWS service = new GestionWSService().getGestionWSPort();
		
		///////////// TESTS DES METHODES DE CRUD AVEC SOAP ////////////
		findArticle(service, 5);
		addAdmin(service,"testSoap@soap.sn","hello");
		listAdmin(service);
		updateCategorie(service, "mode",2);
		System.out.println(service.findCategorie(2).getNomCategorie());
		ArticleCategorie(service,1);
		service.deleteAdmin(3);
		listAdmin(service);
		
	}

}
