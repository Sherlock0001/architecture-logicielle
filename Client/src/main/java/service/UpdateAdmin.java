
package service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Classe Java pour updateAdmin complex type.
 * 
 * <p>
 * Le fragment de schéma suivant indique le contenu attendu figurant dans cette
 * classe.
 * 
 * <pre>
 * &lt;complexType name="updateAdmin">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="admin" type="{http://service/}administrateur" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@SuppressWarnings("restriction")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "updateAdmin", propOrder = { "admin" })
public class UpdateAdmin {

	protected Administrateur admin;

	/**
	 * Obtient la valeur de la propriété admin.
	 * 
	 * @return possible object is {@link Administrateur }
	 * 
	 */
	public Administrateur getAdmin() {
		return admin;
	}

	/**
	 * Définit la valeur de la propriété admin.
	 * 
	 * @param value allowed object is {@link Administrateur }
	 * 
	 */
	public void setAdmin(Administrateur value) {
		this.admin = value;
	}

}
