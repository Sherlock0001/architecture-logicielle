
package service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Classe Java pour categorie complex type.
 * 
 * <p>
 * Le fragment de schéma suivant indique le contenu attendu figurant dans cette
 * classe.
 * 
 * <pre>
 * &lt;complexType name="categorie">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idCategorie" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="nomCategorie" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@SuppressWarnings("restriction")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "categorie", propOrder = { "idCategorie", "nomCategorie" })
public class Categorie {

	protected int idCategorie;
	protected String nomCategorie;

	/**
	 * Obtient la valeur de la propriété idCategorie.
	 * 
	 */
	public int getIdCategorie() {
		return idCategorie;
	}

	/**
	 * Définit la valeur de la propriété idCategorie.
	 * 
	 */
	public void setIdCategorie(int value) {
		this.idCategorie = value;
	}

	/**
	 * Obtient la valeur de la propriété nomCategorie.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getNomCategorie() {
		return nomCategorie;
	}

	/**
	 * Définit la valeur de la propriété nomCategorie.
	 * 
	 * @param value allowed object is {@link String }
	 * 
	 */
	public void setNomCategorie(String value) {
		this.nomCategorie = value;
	}

}
