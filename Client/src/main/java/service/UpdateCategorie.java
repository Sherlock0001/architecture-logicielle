
package service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Classe Java pour updateCategorie complex type.
 * 
 * <p>
 * Le fragment de schéma suivant indique le contenu attendu figurant dans cette
 * classe.
 * 
 * <pre>
 * &lt;complexType name="updateCategorie">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="categorie" type="{http://service/}categorie" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@SuppressWarnings("restriction")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "updateCategorie", propOrder = { "categorie" })
public class UpdateCategorie {

	protected Categorie categorie;

	/**
	 * Obtient la valeur de la propriété categorie.
	 * 
	 * @return possible object is {@link Categorie }
	 * 
	 */
	public Categorie getCategorie() {
		return categorie;
	}

	/**
	 * Définit la valeur de la propriété categorie.
	 * 
	 * @param value allowed object is {@link Categorie }
	 * 
	 */
	public void setCategorie(Categorie value) {
		this.categorie = value;
	}

}
