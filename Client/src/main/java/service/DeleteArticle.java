
package service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Classe Java pour deleteArticle complex type.
 * 
 * <p>
 * Le fragment de schéma suivant indique le contenu attendu figurant dans cette
 * classe.
 * 
 * <pre>
 * &lt;complexType name="deleteArticle">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idArticle" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@SuppressWarnings("restriction")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "deleteArticle", propOrder = { "idArticle" })
public class DeleteArticle {

	protected int idArticle;

	/**
	 * Obtient la valeur de la propriété idArticle.
	 * 
	 */
	public int getIdArticle() {
		return idArticle;
	}

	/**
	 * Définit la valeur de la propriété idArticle.
	 * 
	 */
	public void setIdArticle(int value) {
		this.idArticle = value;
	}

}
