
package service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Classe Java pour addArticle complex type.
 * 
 * <p>
 * Le fragment de schéma suivant indique le contenu attendu figurant dans cette
 * classe.
 * 
 * <pre>
 * &lt;complexType name="addArticle">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="article" type="{http://service/}article" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@SuppressWarnings("restriction")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "addArticle", propOrder = { "article" })
public class AddArticle {

	protected Article article;

	/**
	 * Obtient la valeur de la propriété article.
	 * 
	 * @return possible object is {@link Article }
	 * 
	 */
	public Article getArticle() {
		return article;
	}

	/**
	 * Définit la valeur de la propriété article.
	 * 
	 * @param value allowed object is {@link Article }
	 * 
	 */
	public void setArticle(Article value) {
		this.article = value;
	}

}
