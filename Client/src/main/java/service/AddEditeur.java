
package service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Classe Java pour addEditeur complex type.
 * 
 * <p>
 * Le fragment de schéma suivant indique le contenu attendu figurant dans cette
 * classe.
 * 
 * <pre>
 * &lt;complexType name="addEditeur">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="editeur" type="{http://service/}editeur" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@SuppressWarnings("restriction")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "addEditeur", propOrder = { "editeur" })
public class AddEditeur {

	protected Editeur editeur;

	/**
	 * Obtient la valeur de la propriété editeur.
	 * 
	 * @return possible object is {@link Editeur }
	 * 
	 */
	public Editeur getEditeur() {
		return editeur;
	}

	/**
	 * Définit la valeur de la propriété editeur.
	 * 
	 * @param value allowed object is {@link Editeur }
	 * 
	 */
	public void setEditeur(Editeur value) {
		this.editeur = value;
	}

}
