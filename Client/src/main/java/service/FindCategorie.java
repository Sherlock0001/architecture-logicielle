
package service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Classe Java pour findCategorie complex type.
 * 
 * <p>
 * Le fragment de schéma suivant indique le contenu attendu figurant dans cette
 * classe.
 * 
 * <pre>
 * &lt;complexType name="findCategorie">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idCategorie" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@SuppressWarnings("restriction")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "findCategorie", propOrder = { "idCategorie" })
public class FindCategorie {

	protected int idCategorie;

	/**
	 * Obtient la valeur de la propriété idCategorie.
	 * 
	 */
	public int getIdCategorie() {
		return idCategorie;
	}

	/**
	 * Définit la valeur de la propriété idCategorie.
	 * 
	 */
	public void setIdCategorie(int value) {
		this.idCategorie = value;
	}

}
