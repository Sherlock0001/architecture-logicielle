
package service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Classe Java pour findCategorieResponse complex type.
 * 
 * <p>
 * Le fragment de schéma suivant indique le contenu attendu figurant dans cette
 * classe.
 * 
 * <pre>
 * &lt;complexType name="findCategorieResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="return" type="{http://service/}categorie" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@SuppressWarnings("restriction")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "findCategorieResponse", propOrder = { "_return" })
public class FindCategorieResponse {

	@XmlElement(name = "return")
	protected Categorie _return;

	/**
	 * Obtient la valeur de la propriété return.
	 * 
	 * @return possible object is {@link Categorie }
	 * 
	 */
	public Categorie getReturn() {
		return _return;
	}

	/**
	 * Définit la valeur de la propriété return.
	 * 
	 * @param value allowed object is {@link Categorie }
	 * 
	 */
	public void setReturn(Categorie value) {
		this._return = value;
	}

}
