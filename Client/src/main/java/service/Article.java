
package service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Classe Java pour article complex type.
 * 
 * <p>
 * Le fragment de schéma suivant indique le contenu attendu figurant dans cette
 * classe.
 * 
 * <pre>
 * &lt;complexType name="article">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="auteur" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="categorie" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idArticle" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="image" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="titre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@SuppressWarnings("restriction")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "article", propOrder = { "auteur", "categorie", "description", "idArticle", "image", "titre" })
public class Article {

	protected String auteur;
	protected int categorie;
	protected String description;
	protected int idArticle;
	protected String image;
	protected String titre;

	/**
	 * Obtient la valeur de la propriété auteur.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAuteur() {
		return auteur;
	}

	/**
	 * Définit la valeur de la propriété auteur.
	 * 
	 * @param value allowed object is {@link String }
	 * 
	 */
	public void setAuteur(String value) {
		this.auteur = value;
	}

	/**
	 * Obtient la valeur de la propriété categorie.
	 * 
	 */
	public int getCategorie() {
		return categorie;
	}

	/**
	 * Définit la valeur de la propriété categorie.
	 * 
	 */
	public void setCategorie(int value) {
		this.categorie = value;
	}

	/**
	 * Obtient la valeur de la propriété description.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Définit la valeur de la propriété description.
	 * 
	 * @param value allowed object is {@link String }
	 * 
	 */
	public void setDescription(String value) {
		this.description = value;
	}

	/**
	 * Obtient la valeur de la propriété idArticle.
	 * 
	 */
	public int getIdArticle() {
		return idArticle;
	}

	/**
	 * Définit la valeur de la propriété idArticle.
	 * 
	 */
	public void setIdArticle(int value) {
		this.idArticle = value;
	}

	/**
	 * Obtient la valeur de la propriété image.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getImage() {
		return image;
	}

	/**
	 * Définit la valeur de la propriété image.
	 * 
	 * @param value allowed object is {@link String }
	 * 
	 */
	public void setImage(String value) {
		this.image = value;
	}

	/**
	 * Obtient la valeur de la propriété titre.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTitre() {
		return titre;
	}

	/**
	 * Définit la valeur de la propriété titre.
	 * 
	 * @param value allowed object is {@link String }
	 * 
	 */
	public void setTitre(String value) {
		this.titre = value;
	}

}
