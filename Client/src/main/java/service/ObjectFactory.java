
package service;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

/**
 * This object contains factory methods for each Java content interface and Java
 * element interface generated in the service package.
 * <p>
 * An ObjectFactory allows you to programatically construct new instances of the
 * Java representation for XML content. The Java representation of XML content
 * can consist of schema derived interfaces and classes representing the binding
 * of schema type definitions, element declarations and model groups. Factory
 * methods for each of these are provided in this class.
 * 
 */
@SuppressWarnings("restriction")
@XmlRegistry
public class ObjectFactory {

	private final static QName _UpdateAdmin_QNAME = new QName("http://service/", "updateAdmin");
	private final static QName _DeleteAdminResponse_QNAME = new QName("http://service/", "deleteAdminResponse");
	private final static QName _FindArticleCategoryResponse_QNAME = new QName("http://service/",
			"findArticleCategoryResponse");
	private final static QName _AddEditeur_QNAME = new QName("http://service/", "addEditeur");
	private final static QName _FindAdmin_QNAME = new QName("http://service/", "findAdmin");
	private final static QName _UpdateEditeur_QNAME = new QName("http://service/", "updateEditeur");
	private final static QName _UpdateCategorieResponse_QNAME = new QName("http://service/", "updateCategorieResponse");
	private final static QName _ListCategorieResponse_QNAME = new QName("http://service/", "listCategorieResponse");
	private final static QName _DeleteArticle_QNAME = new QName("http://service/", "deleteArticle");
	private final static QName _DeleteArticleResponse_QNAME = new QName("http://service/", "deleteArticleResponse");
	private final static QName _FindCategorieResponse_QNAME = new QName("http://service/", "findCategorieResponse");
	private final static QName _ListEditeurResponse_QNAME = new QName("http://service/", "listEditeurResponse");
	private final static QName _FindArticleCategory_QNAME = new QName("http://service/", "findArticleCategory");
	private final static QName _ListEditeur_QNAME = new QName("http://service/", "listEditeur");
	private final static QName _FindEditeurResponse_QNAME = new QName("http://service/", "findEditeurResponse");
	private final static QName _FindCategorie_QNAME = new QName("http://service/", "findCategorie");
	private final static QName _FindEditeur_QNAME = new QName("http://service/", "findEditeur");
	private final static QName _AddAdminResponse_QNAME = new QName("http://service/", "addAdminResponse");
	private final static QName _AddCategorieResponse_QNAME = new QName("http://service/", "addCategorieResponse");
	private final static QName _FindArticleResponse_QNAME = new QName("http://service/", "findArticleResponse");
	private final static QName _AddArticleResponse_QNAME = new QName("http://service/", "addArticleResponse");
	private final static QName _FindAdminResponse_QNAME = new QName("http://service/", "findAdminResponse");
	private final static QName _UpdateEditeurResponse_QNAME = new QName("http://service/", "updateEditeurResponse");
	private final static QName _DeleteCategorie_QNAME = new QName("http://service/", "deleteCategorie");
	private final static QName _UpdateAdminResponse_QNAME = new QName("http://service/", "updateAdminResponse");
	private final static QName _AddAdmin_QNAME = new QName("http://service/", "addAdmin");
	private final static QName _AddArticle_QNAME = new QName("http://service/", "addArticle");
	private final static QName _UpdateArticle_QNAME = new QName("http://service/", "updateArticle");
	private final static QName _UpdateCategorie_QNAME = new QName("http://service/", "updateCategorie");
	private final static QName _DeleteEditeur_QNAME = new QName("http://service/", "deleteEditeur");
	private final static QName _AddEditeurResponse_QNAME = new QName("http://service/", "addEditeurResponse");
	private final static QName _ListAdmin_QNAME = new QName("http://service/", "listAdmin");
	private final static QName _UpdateArticleResponse_QNAME = new QName("http://service/", "updateArticleResponse");
	private final static QName _AddCategorie_QNAME = new QName("http://service/", "addCategorie");
	private final static QName _ListArticle_QNAME = new QName("http://service/", "listArticle");
	private final static QName _DeleteEditeurResponse_QNAME = new QName("http://service/", "deleteEditeurResponse");
	private final static QName _ListArticleResponse_QNAME = new QName("http://service/", "listArticleResponse");
	private final static QName _ListCategorie_QNAME = new QName("http://service/", "listCategorie");
	private final static QName _DeleteAdmin_QNAME = new QName("http://service/", "deleteAdmin");
	private final static QName _FindArticle_QNAME = new QName("http://service/", "findArticle");
	private final static QName _DeleteCategorieResponse_QNAME = new QName("http://service/", "deleteCategorieResponse");
	private final static QName _ListAdminResponse_QNAME = new QName("http://service/", "listAdminResponse");

	/**
	 * Create a new ObjectFactory that can be used to create new instances of schema
	 * derived classes for package: service
	 * 
	 */
	public ObjectFactory() {
	}

	/**
	 * Create an instance of {@link UpdateCategorie }
	 * 
	 */
	public UpdateCategorie createUpdateCategorie() {
		return new UpdateCategorie();
	}

	/**
	 * Create an instance of {@link AddArticle }
	 * 
	 */
	public AddArticle createAddArticle() {
		return new AddArticle();
	}

	/**
	 * Create an instance of {@link UpdateArticle }
	 * 
	 */
	public UpdateArticle createUpdateArticle() {
		return new UpdateArticle();
	}

	/**
	 * Create an instance of {@link AddAdmin }
	 * 
	 */
	public AddAdmin createAddAdmin() {
		return new AddAdmin();
	}

	/**
	 * Create an instance of {@link UpdateAdminResponse }
	 * 
	 */
	public UpdateAdminResponse createUpdateAdminResponse() {
		return new UpdateAdminResponse();
	}

	/**
	 * Create an instance of {@link ListArticleResponse }
	 * 
	 */
	public ListArticleResponse createListArticleResponse() {
		return new ListArticleResponse();
	}

	/**
	 * Create an instance of {@link DeleteEditeurResponse }
	 * 
	 */
	public DeleteEditeurResponse createDeleteEditeurResponse() {
		return new DeleteEditeurResponse();
	}

	/**
	 * Create an instance of {@link AddCategorie }
	 * 
	 */
	public AddCategorie createAddCategorie() {
		return new AddCategorie();
	}

	/**
	 * Create an instance of {@link ListArticle }
	 * 
	 */
	public ListArticle createListArticle() {
		return new ListArticle();
	}

	/**
	 * Create an instance of {@link AddEditeurResponse }
	 * 
	 */
	public AddEditeurResponse createAddEditeurResponse() {
		return new AddEditeurResponse();
	}

	/**
	 * Create an instance of {@link ListAdmin }
	 * 
	 */
	public ListAdmin createListAdmin() {
		return new ListAdmin();
	}

	/**
	 * Create an instance of {@link UpdateArticleResponse }
	 * 
	 */
	public UpdateArticleResponse createUpdateArticleResponse() {
		return new UpdateArticleResponse();
	}

	/**
	 * Create an instance of {@link DeleteEditeur }
	 * 
	 */
	public DeleteEditeur createDeleteEditeur() {
		return new DeleteEditeur();
	}

	/**
	 * Create an instance of {@link DeleteAdmin }
	 * 
	 */
	public DeleteAdmin createDeleteAdmin() {
		return new DeleteAdmin();
	}

	/**
	 * Create an instance of {@link FindArticle }
	 * 
	 */
	public FindArticle createFindArticle() {
		return new FindArticle();
	}

	/**
	 * Create an instance of {@link ListCategorie }
	 * 
	 */
	public ListCategorie createListCategorie() {
		return new ListCategorie();
	}

	/**
	 * Create an instance of {@link ListAdminResponse }
	 * 
	 */
	public ListAdminResponse createListAdminResponse() {
		return new ListAdminResponse();
	}

	/**
	 * Create an instance of {@link DeleteCategorieResponse }
	 * 
	 */
	public DeleteCategorieResponse createDeleteCategorieResponse() {
		return new DeleteCategorieResponse();
	}

	/**
	 * Create an instance of {@link ListCategorieResponse }
	 * 
	 */
	public ListCategorieResponse createListCategorieResponse() {
		return new ListCategorieResponse();
	}

	/**
	 * Create an instance of {@link UpdateCategorieResponse }
	 * 
	 */
	public UpdateCategorieResponse createUpdateCategorieResponse() {
		return new UpdateCategorieResponse();
	}

	/**
	 * Create an instance of {@link AddEditeur }
	 * 
	 */
	public AddEditeur createAddEditeur() {
		return new AddEditeur();
	}

	/**
	 * Create an instance of {@link FindAdmin }
	 * 
	 */
	public FindAdmin createFindAdmin() {
		return new FindAdmin();
	}

	/**
	 * Create an instance of {@link UpdateEditeur }
	 * 
	 */
	public UpdateEditeur createUpdateEditeur() {
		return new UpdateEditeur();
	}

	/**
	 * Create an instance of {@link DeleteAdminResponse }
	 * 
	 */
	public DeleteAdminResponse createDeleteAdminResponse() {
		return new DeleteAdminResponse();
	}

	/**
	 * Create an instance of {@link FindArticleCategoryResponse }
	 * 
	 */
	public FindArticleCategoryResponse createFindArticleCategoryResponse() {
		return new FindArticleCategoryResponse();
	}

	/**
	 * Create an instance of {@link UpdateAdmin }
	 * 
	 */
	public UpdateAdmin createUpdateAdmin() {
		return new UpdateAdmin();
	}

	/**
	 * Create an instance of {@link FindEditeurResponse }
	 * 
	 */
	public FindEditeurResponse createFindEditeurResponse() {
		return new FindEditeurResponse();
	}

	/**
	 * Create an instance of {@link FindArticleCategory }
	 * 
	 */
	public FindArticleCategory createFindArticleCategory() {
		return new FindArticleCategory();
	}

	/**
	 * Create an instance of {@link ListEditeur }
	 * 
	 */
	public ListEditeur createListEditeur() {
		return new ListEditeur();
	}

	/**
	 * Create an instance of {@link ListEditeurResponse }
	 * 
	 */
	public ListEditeurResponse createListEditeurResponse() {
		return new ListEditeurResponse();
	}

	/**
	 * Create an instance of {@link DeleteArticleResponse }
	 * 
	 */
	public DeleteArticleResponse createDeleteArticleResponse() {
		return new DeleteArticleResponse();
	}

	/**
	 * Create an instance of {@link FindCategorieResponse }
	 * 
	 */
	public FindCategorieResponse createFindCategorieResponse() {
		return new FindCategorieResponse();
	}

	/**
	 * Create an instance of {@link DeleteArticle }
	 * 
	 */
	public DeleteArticle createDeleteArticle() {
		return new DeleteArticle();
	}

	/**
	 * Create an instance of {@link AddAdminResponse }
	 * 
	 */
	public AddAdminResponse createAddAdminResponse() {
		return new AddAdminResponse();
	}

	/**
	 * Create an instance of {@link FindCategorie }
	 * 
	 */
	public FindCategorie createFindCategorie() {
		return new FindCategorie();
	}

	/**
	 * Create an instance of {@link FindEditeur }
	 * 
	 */
	public FindEditeur createFindEditeur() {
		return new FindEditeur();
	}

	/**
	 * Create an instance of {@link DeleteCategorie }
	 * 
	 */
	public DeleteCategorie createDeleteCategorie() {
		return new DeleteCategorie();
	}

	/**
	 * Create an instance of {@link AddArticleResponse }
	 * 
	 */
	public AddArticleResponse createAddArticleResponse() {
		return new AddArticleResponse();
	}

	/**
	 * Create an instance of {@link FindAdminResponse }
	 * 
	 */
	public FindAdminResponse createFindAdminResponse() {
		return new FindAdminResponse();
	}

	/**
	 * Create an instance of {@link UpdateEditeurResponse }
	 * 
	 */
	public UpdateEditeurResponse createUpdateEditeurResponse() {
		return new UpdateEditeurResponse();
	}

	/**
	 * Create an instance of {@link FindArticleResponse }
	 * 
	 */
	public FindArticleResponse createFindArticleResponse() {
		return new FindArticleResponse();
	}

	/**
	 * Create an instance of {@link AddCategorieResponse }
	 * 
	 */
	public AddCategorieResponse createAddCategorieResponse() {
		return new AddCategorieResponse();
	}

	/**
	 * Create an instance of {@link Administrateur }
	 * 
	 */
	public Administrateur createAdministrateur() {
		return new Administrateur();
	}

	/**
	 * Create an instance of {@link Editeur }
	 * 
	 */
	public Editeur createEditeur() {
		return new Editeur();
	}

	/**
	 * Create an instance of {@link Categorie }
	 * 
	 */
	public Categorie createCategorie() {
		return new Categorie();
	}

	/**
	 * Create an instance of {@link Article }
	 * 
	 */
	public Article createArticle() {
		return new Article();
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link UpdateAdmin
	 * }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://service/", name = "updateAdmin")
	public JAXBElement<UpdateAdmin> createUpdateAdmin(UpdateAdmin value) {
		return new JAXBElement<UpdateAdmin>(_UpdateAdmin_QNAME, UpdateAdmin.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link DeleteAdminResponse
	 * }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://service/", name = "deleteAdminResponse")
	public JAXBElement<DeleteAdminResponse> createDeleteAdminResponse(DeleteAdminResponse value) {
		return new JAXBElement<DeleteAdminResponse>(_DeleteAdminResponse_QNAME, DeleteAdminResponse.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement
	 * }{@code <}{@link FindArticleCategoryResponse }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://service/", name = "findArticleCategoryResponse")
	public JAXBElement<FindArticleCategoryResponse> createFindArticleCategoryResponse(
			FindArticleCategoryResponse value) {
		return new JAXBElement<FindArticleCategoryResponse>(_FindArticleCategoryResponse_QNAME,
				FindArticleCategoryResponse.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link AddEditeur
	 * }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://service/", name = "addEditeur")
	public JAXBElement<AddEditeur> createAddEditeur(AddEditeur value) {
		return new JAXBElement<AddEditeur>(_AddEditeur_QNAME, AddEditeur.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link FindAdmin
	 * }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://service/", name = "findAdmin")
	public JAXBElement<FindAdmin> createFindAdmin(FindAdmin value) {
		return new JAXBElement<FindAdmin>(_FindAdmin_QNAME, FindAdmin.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link UpdateEditeur
	 * }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://service/", name = "updateEditeur")
	public JAXBElement<UpdateEditeur> createUpdateEditeur(UpdateEditeur value) {
		return new JAXBElement<UpdateEditeur>(_UpdateEditeur_QNAME, UpdateEditeur.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement
	 * }{@code <}{@link UpdateCategorieResponse }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://service/", name = "updateCategorieResponse")
	public JAXBElement<UpdateCategorieResponse> createUpdateCategorieResponse(UpdateCategorieResponse value) {
		return new JAXBElement<UpdateCategorieResponse>(_UpdateCategorieResponse_QNAME, UpdateCategorieResponse.class,
				null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement
	 * }{@code <}{@link ListCategorieResponse }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://service/", name = "listCategorieResponse")
	public JAXBElement<ListCategorieResponse> createListCategorieResponse(ListCategorieResponse value) {
		return new JAXBElement<ListCategorieResponse>(_ListCategorieResponse_QNAME, ListCategorieResponse.class, null,
				value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link DeleteArticle
	 * }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://service/", name = "deleteArticle")
	public JAXBElement<DeleteArticle> createDeleteArticle(DeleteArticle value) {
		return new JAXBElement<DeleteArticle>(_DeleteArticle_QNAME, DeleteArticle.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement
	 * }{@code <}{@link DeleteArticleResponse }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://service/", name = "deleteArticleResponse")
	public JAXBElement<DeleteArticleResponse> createDeleteArticleResponse(DeleteArticleResponse value) {
		return new JAXBElement<DeleteArticleResponse>(_DeleteArticleResponse_QNAME, DeleteArticleResponse.class, null,
				value);
	}

	/**
	 * Create an instance of {@link JAXBElement
	 * }{@code <}{@link FindCategorieResponse }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://service/", name = "findCategorieResponse")
	public JAXBElement<FindCategorieResponse> createFindCategorieResponse(FindCategorieResponse value) {
		return new JAXBElement<FindCategorieResponse>(_FindCategorieResponse_QNAME, FindCategorieResponse.class, null,
				value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link ListEditeurResponse
	 * }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://service/", name = "listEditeurResponse")
	public JAXBElement<ListEditeurResponse> createListEditeurResponse(ListEditeurResponse value) {
		return new JAXBElement<ListEditeurResponse>(_ListEditeurResponse_QNAME, ListEditeurResponse.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link FindArticleCategory
	 * }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://service/", name = "findArticleCategory")
	public JAXBElement<FindArticleCategory> createFindArticleCategory(FindArticleCategory value) {
		return new JAXBElement<FindArticleCategory>(_FindArticleCategory_QNAME, FindArticleCategory.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link ListEditeur
	 * }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://service/", name = "listEditeur")
	public JAXBElement<ListEditeur> createListEditeur(ListEditeur value) {
		return new JAXBElement<ListEditeur>(_ListEditeur_QNAME, ListEditeur.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link FindEditeurResponse
	 * }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://service/", name = "findEditeurResponse")
	public JAXBElement<FindEditeurResponse> createFindEditeurResponse(FindEditeurResponse value) {
		return new JAXBElement<FindEditeurResponse>(_FindEditeurResponse_QNAME, FindEditeurResponse.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link FindCategorie
	 * }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://service/", name = "findCategorie")
	public JAXBElement<FindCategorie> createFindCategorie(FindCategorie value) {
		return new JAXBElement<FindCategorie>(_FindCategorie_QNAME, FindCategorie.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link FindEditeur
	 * }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://service/", name = "findEditeur")
	public JAXBElement<FindEditeur> createFindEditeur(FindEditeur value) {
		return new JAXBElement<FindEditeur>(_FindEditeur_QNAME, FindEditeur.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link AddAdminResponse
	 * }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://service/", name = "addAdminResponse")
	public JAXBElement<AddAdminResponse> createAddAdminResponse(AddAdminResponse value) {
		return new JAXBElement<AddAdminResponse>(_AddAdminResponse_QNAME, AddAdminResponse.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement
	 * }{@code <}{@link AddCategorieResponse }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://service/", name = "addCategorieResponse")
	public JAXBElement<AddCategorieResponse> createAddCategorieResponse(AddCategorieResponse value) {
		return new JAXBElement<AddCategorieResponse>(_AddCategorieResponse_QNAME, AddCategorieResponse.class, null,
				value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link FindArticleResponse
	 * }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://service/", name = "findArticleResponse")
	public JAXBElement<FindArticleResponse> createFindArticleResponse(FindArticleResponse value) {
		return new JAXBElement<FindArticleResponse>(_FindArticleResponse_QNAME, FindArticleResponse.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link AddArticleResponse
	 * }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://service/", name = "addArticleResponse")
	public JAXBElement<AddArticleResponse> createAddArticleResponse(AddArticleResponse value) {
		return new JAXBElement<AddArticleResponse>(_AddArticleResponse_QNAME, AddArticleResponse.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link FindAdminResponse
	 * }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://service/", name = "findAdminResponse")
	public JAXBElement<FindAdminResponse> createFindAdminResponse(FindAdminResponse value) {
		return new JAXBElement<FindAdminResponse>(_FindAdminResponse_QNAME, FindAdminResponse.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement
	 * }{@code <}{@link UpdateEditeurResponse }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://service/", name = "updateEditeurResponse")
	public JAXBElement<UpdateEditeurResponse> createUpdateEditeurResponse(UpdateEditeurResponse value) {
		return new JAXBElement<UpdateEditeurResponse>(_UpdateEditeurResponse_QNAME, UpdateEditeurResponse.class, null,
				value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link DeleteCategorie
	 * }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://service/", name = "deleteCategorie")
	public JAXBElement<DeleteCategorie> createDeleteCategorie(DeleteCategorie value) {
		return new JAXBElement<DeleteCategorie>(_DeleteCategorie_QNAME, DeleteCategorie.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link UpdateAdminResponse
	 * }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://service/", name = "updateAdminResponse")
	public JAXBElement<UpdateAdminResponse> createUpdateAdminResponse(UpdateAdminResponse value) {
		return new JAXBElement<UpdateAdminResponse>(_UpdateAdminResponse_QNAME, UpdateAdminResponse.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link AddAdmin
	 * }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://service/", name = "addAdmin")
	public JAXBElement<AddAdmin> createAddAdmin(AddAdmin value) {
		return new JAXBElement<AddAdmin>(_AddAdmin_QNAME, AddAdmin.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link AddArticle
	 * }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://service/", name = "addArticle")
	public JAXBElement<AddArticle> createAddArticle(AddArticle value) {
		return new JAXBElement<AddArticle>(_AddArticle_QNAME, AddArticle.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link UpdateArticle
	 * }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://service/", name = "updateArticle")
	public JAXBElement<UpdateArticle> createUpdateArticle(UpdateArticle value) {
		return new JAXBElement<UpdateArticle>(_UpdateArticle_QNAME, UpdateArticle.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link UpdateCategorie
	 * }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://service/", name = "updateCategorie")
	public JAXBElement<UpdateCategorie> createUpdateCategorie(UpdateCategorie value) {
		return new JAXBElement<UpdateCategorie>(_UpdateCategorie_QNAME, UpdateCategorie.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link DeleteEditeur
	 * }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://service/", name = "deleteEditeur")
	public JAXBElement<DeleteEditeur> createDeleteEditeur(DeleteEditeur value) {
		return new JAXBElement<DeleteEditeur>(_DeleteEditeur_QNAME, DeleteEditeur.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link AddEditeurResponse
	 * }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://service/", name = "addEditeurResponse")
	public JAXBElement<AddEditeurResponse> createAddEditeurResponse(AddEditeurResponse value) {
		return new JAXBElement<AddEditeurResponse>(_AddEditeurResponse_QNAME, AddEditeurResponse.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link ListAdmin
	 * }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://service/", name = "listAdmin")
	public JAXBElement<ListAdmin> createListAdmin(ListAdmin value) {
		return new JAXBElement<ListAdmin>(_ListAdmin_QNAME, ListAdmin.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement
	 * }{@code <}{@link UpdateArticleResponse }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://service/", name = "updateArticleResponse")
	public JAXBElement<UpdateArticleResponse> createUpdateArticleResponse(UpdateArticleResponse value) {
		return new JAXBElement<UpdateArticleResponse>(_UpdateArticleResponse_QNAME, UpdateArticleResponse.class, null,
				value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link AddCategorie
	 * }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://service/", name = "addCategorie")
	public JAXBElement<AddCategorie> createAddCategorie(AddCategorie value) {
		return new JAXBElement<AddCategorie>(_AddCategorie_QNAME, AddCategorie.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link ListArticle
	 * }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://service/", name = "listArticle")
	public JAXBElement<ListArticle> createListArticle(ListArticle value) {
		return new JAXBElement<ListArticle>(_ListArticle_QNAME, ListArticle.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement
	 * }{@code <}{@link DeleteEditeurResponse }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://service/", name = "deleteEditeurResponse")
	public JAXBElement<DeleteEditeurResponse> createDeleteEditeurResponse(DeleteEditeurResponse value) {
		return new JAXBElement<DeleteEditeurResponse>(_DeleteEditeurResponse_QNAME, DeleteEditeurResponse.class, null,
				value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link ListArticleResponse
	 * }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://service/", name = "listArticleResponse")
	public JAXBElement<ListArticleResponse> createListArticleResponse(ListArticleResponse value) {
		return new JAXBElement<ListArticleResponse>(_ListArticleResponse_QNAME, ListArticleResponse.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link ListCategorie
	 * }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://service/", name = "listCategorie")
	public JAXBElement<ListCategorie> createListCategorie(ListCategorie value) {
		return new JAXBElement<ListCategorie>(_ListCategorie_QNAME, ListCategorie.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link DeleteAdmin
	 * }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://service/", name = "deleteAdmin")
	public JAXBElement<DeleteAdmin> createDeleteAdmin(DeleteAdmin value) {
		return new JAXBElement<DeleteAdmin>(_DeleteAdmin_QNAME, DeleteAdmin.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link FindArticle
	 * }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://service/", name = "findArticle")
	public JAXBElement<FindArticle> createFindArticle(FindArticle value) {
		return new JAXBElement<FindArticle>(_FindArticle_QNAME, FindArticle.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement
	 * }{@code <}{@link DeleteCategorieResponse }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://service/", name = "deleteCategorieResponse")
	public JAXBElement<DeleteCategorieResponse> createDeleteCategorieResponse(DeleteCategorieResponse value) {
		return new JAXBElement<DeleteCategorieResponse>(_DeleteCategorieResponse_QNAME, DeleteCategorieResponse.class,
				null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link ListAdminResponse
	 * }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = "http://service/", name = "listAdminResponse")
	public JAXBElement<ListAdminResponse> createListAdminResponse(ListAdminResponse value) {
		return new JAXBElement<ListAdminResponse>(_ListAdminResponse_QNAME, ListAdminResponse.class, null, value);
	}

}
