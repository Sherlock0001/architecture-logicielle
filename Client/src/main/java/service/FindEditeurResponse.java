
package service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Classe Java pour findEditeurResponse complex type.
 * 
 * <p>
 * Le fragment de schéma suivant indique le contenu attendu figurant dans cette
 * classe.
 * 
 * <pre>
 * &lt;complexType name="findEditeurResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="return" type="{http://service/}editeur" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@SuppressWarnings("restriction")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "findEditeurResponse", propOrder = { "_return" })
public class FindEditeurResponse {

	@XmlElement(name = "return")
	protected Editeur _return;

	/**
	 * Obtient la valeur de la propriété return.
	 * 
	 * @return possible object is {@link Editeur }
	 * 
	 */
	public Editeur getReturn() {
		return _return;
	}

	/**
	 * Définit la valeur de la propriété return.
	 * 
	 * @param value allowed object is {@link Editeur }
	 * 
	 */
	public void setReturn(Editeur value) {
		this._return = value;
	}

}
